﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using WebApiTemplate.Interfaces;
using WebApiTemplate.Entities;

namespace WebApiTemplate.Mappings
{
    public class EntityMap : ClassMap<Entity>
    {

        public EntityMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
        }
    }
}