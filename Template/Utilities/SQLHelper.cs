﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Cfg;
using System.Configuration;
using NHibernate;
using FluentNHibernate.Cfg.Db;
using NHibernate.Tool.hbm2ddl;

namespace WebApiTemplate.Utilities
{
    public class SQLHelper
    {

        public static String GetConnectionString(string connectionString)
        {
            return ConfigurationManager.ConnectionStrings[connectionString].ConnectionString;
        }

        private static FluentConfiguration GetConfiguration(string connection)
        {
            var connectionString = GetConnectionString(connection);

            return Fluently.Configure()
                .Database(
                    MsSqlConfiguration.MsSql2008
                        .ConnectionString(connectionString)
                            ).Mappings(m => m.FluentMappings.AddFromAssemblyOf<WebApiTemplate.Mappings.EntityMap>());
        }


        public static ISessionFactory CreateSessionFactory(string connectionString)
        {
            var config = GetConfiguration(connectionString);

            return config.BuildSessionFactory();
        }


        public static void CreateDatabase(string connectionString)
        {
            var config = GetConfiguration(connectionString);

            config.ExposeConfiguration(
                      c => new SchemaExport(c).Execute(true, true, false))
                 .BuildConfiguration();
        }
    }
}