﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiTemplate.Interfaces;

namespace WebApiTemplate.Entities
{
    public class Entity : IEntity
    {
        public virtual int Id { get; set; }
        public virtual String Name { get; set; }


        public Entity() { }
    }
}