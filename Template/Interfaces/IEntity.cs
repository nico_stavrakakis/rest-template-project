﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebApiTemplate.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
        String Name { get; set; }
    }
}
